# README #

This is the source code for the Digital-ROX project,for the Cyber Infrastructure class Spring 2016

### Classes ###

* Diego.java : contains the source code as seen in class
* App.java: contains the main method where the parsed data and ontology are integrated
* OntologyReader.java: Contains all the functions for displaying Classes, Individuals, Data Properties, and Object Properties.
* OntologyManager.java: Contains all the methods for adding Individuals, Data Properties, Object Properties, etc.
* GeoWeatherParser.java: Contains the code for parsing and storing the info in a datastructure


//TODO addObjectProperty()      needs implementing

//TODO addObjectPropertyValue() needs implementing 

//TODO - debug the data property instances type e.g xsd:string, etc. In the XML parsing



### Objectives ###

* Parse XML files
* Populate our current existing ontology with information from the XML files

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Erick Garcia <egarcia87@miners.utep.edu>
* Smriti
* Georgia Almodovar
* Richard Samples
* Perry Houser