package cs.utep.owlApi_test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyIRIMapper;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.AutoIRIMapper;

import cs.utep.xmlParser.GeoWeatherParser;

public class App {
  public static void main(String[] args)
      throws OWLOntologyStorageException, IOException, OWLOntologyCreationException {
    // Creating our ontology reader to display contents
    OntologyReader reader = new OntologyReader();
    // Creating our ontology manager and
    OntologyManager source_ontology = new OntologyManager("GeoTimeScale2","GeoTimeScale2.owl", "http://www.semanticweb.org/smriti/ontologies/2016/3/GeoWeatherReport");
    reader.showContents(source_ontology);
    source_ontology.importOntology("OriginalOntologies/");
   
    // // Adding parsed data to ontology
     GeoWeatherParser weather_xml = new GeoWeatherParser("UTmergedfile.xml");

     //Populating the WeatherReport Individual with Data Properties
     int counter = 0;
     for (String key : weather_xml.getWeatherReports().keySet()) {
     source_ontology.addIndividual("WeatherReport", "WeatherReport" + key);
     ArrayList<String> elements = weather_xml.getWeatherReports().get(key);
      for(int x = 0; x < elements.size(); x = x+3){
        String id = elements.get(x);
        String value = elements.get(x+1);
        String type = elements.get(x+2);
        source_ontology.addDataPropertyValue("WeatherReport", "WeatherReport" + key, id, value, type);
        counter ++;
      }
     }

     //Locations Data Properties
     for (String key : weather_xml.getLocations().keySet()) {
     source_ontology.addIndividual("Location", "Location" + key);
     ArrayList<String> location = weather_xml.getLocations().get(key);
      for(int x = 0; x < location.size(); x = x+3){
        String id = location.get(x);
        String value = location.get(x+1);
        String type = location.get(x+2);
        source_ontology.addDataPropertyValue("Location", "Location" + key, id, value, type);
      }
     }
     
     //WindSpeeds data Properties
     for (String key : weather_xml.getWindSpeeds().keySet()) {
     source_ontology.addIndividual("WindSpeed", "WindSpeed" + key);
     ArrayList<String> windSpeeds = weather_xml.getWindSpeeds().get(key);
      for(int x = 0; x < windSpeeds.size(); x = x+3){
        String id = windSpeeds.get(x);
        String value = windSpeeds.get(x+1);
        String type = windSpeeds.get(x+2);
        source_ontology.addDataPropertyValue("WindSpeed", "WindSpeed" + key, id, value, type);
      }
     }
     
     //Temperature Data Properties
     for (String key : weather_xml.getTemperatures().keySet()) {
     source_ontology.addIndividual("Temperature", "Temperature" + key + "F");
     source_ontology.addIndividual("Temperature", "Temperature" + key + "C");
     ArrayList<String> temperatures = weather_xml.getTemperatures().get(key);
      for(int x = 0; x < temperatures.size(); x = x+6){
        String id = temperatures.get(x);
        String value = temperatures.get(x+1);
        String type = temperatures.get(x+2);
        source_ontology.addDataPropertyValue("Temperature", "Temperature" + key + "F", id, value, type); //Farenheit
        id = temperatures.get(x+3);
        value = temperatures.get(x+4);
        type = temperatures.get(x+5);
        source_ontology.addDataPropertyValue("Temperature", "Temperature" + key + "C", id, value, type); //Celcius
      }
     }

     //Adding Location hasWeatherReport Object Property
     for (int i = 0; i < counter; i++){
       source_ontology.addObjectPropertyValue("Location", "Location" + i, "WeatherReport", "WeatherReport" + i , "hasWeatherReport");
       source_ontology.addObjectPropertyValue("Location", "Location" + i, "Units", "degrees", "hasUnits");
     }

     //Adding WeatherReport hasWindSpeed Object Property
     for (int i = 0; i < counter; i++){
       source_ontology.addObjectPropertyValue("WeatherReport", "WeatherReport" + i, "WindSpeed", "WindSpeed" + i , "hasWindSpeed");
       source_ontology.addObjectPropertyValue("WindSpeed", "WindSpeed" + i, "Units", "mph", "hasUnits");
     }
     
     //Adding WeatherReport hasTemperature Object Property
     for (int i = 0; i < counter; i++){
       source_ontology.addObjectPropertyValue("WeatherReport", "WeatherReport" + i, "Temperature", "Temperature" + i + "F", "hasTemperature");
       source_ontology.addObjectPropertyValue("Temperature", "Temperature" + i + "F", "Units", "farenheit", "hasUnits");
       source_ontology.addObjectPropertyValue("WeatherReport", "WeatherReport" + i, "Temperature", "Temperature" + i + "C", "hasTemperature");
       source_ontology.addObjectPropertyValue("Temperature", "Temperature" + i + "C", "Units", "celcius", "hasUnits");
     }

    source_ontology.saveChanges("GeoTimeScale2.owl");
//    OntologyManager output = new OntologyManager("GeoWeatherReport","GeoWeatherReportOutput.owl", "http://www.semanticweb.org/smriti/ontologies/2016/4/");
//    reader.showContents(output);
  }
}
