package cs.utep.owlApi_test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyIRIMapper;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.AutoIRIMapper;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import uk.ac.manchester.cs.owl.owlapi.OWL2DatatypeImpl;

public class OntologyManager {
	private String ontologyName;

	public String getOntologyName() {
		return ontologyName;
	}

	public OWLDataFactory getFactory() {
		return factory;
	}

	private OWLOntologyManager manager;
	private OWLOntology ontology;
	private OWLDataFactory factory;
	private Set<OWLClass> classes;
	private Set<OWLNamedIndividual> individuals;
	private Set<OWLDataProperty> dataProperties;
	private Set<OWLObjectProperty> objectProperties;
	private String uri;

	public OntologyManager(String ontologyName, String ontologyFileName, String prefix) throws OWLOntologyCreationException {
		File file = new File(ontologyFileName);
		// uri = "file:///" + this.ontologyName;

		// Constructing our ontology Manager
		this.ontologyName = ontologyName;
//		this.uri = prefix + this.ontologyName + ".owl"; Example
//  this.uri = prefix + this.ontologyName; 
  this.uri = prefix; 
		this.manager = OWLManager.createOWLOntologyManager();
		//Adding an IRI Mapper
    File geoWeatherReportOnt = new File("Original Ontologies");
    OWLOntologyIRIMapper autoIRIMapper = new AutoIRIMapper(geoWeatherReportOnt, false);
    this.manager.addIRIMapper(autoIRIMapper);
		
		this.ontology = this.manager.loadOntologyFromOntologyDocument(file);
		this.factory = this.manager.getOWLDataFactory();
		this.classes = this.ontology.getClassesInSignature();
		this.individuals = this.ontology.getIndividualsInSignature();
		this.dataProperties = this.ontology.getDataPropertiesInSignature();
		this.objectProperties = this.ontology.getObjectPropertiesInSignature();
	}
	
	public void importOntology(String importOntology){
	  File geoWeatherReportOnt = new File(importOntology);
    OWLOntologyIRIMapper autoIRIMapper = new AutoIRIMapper(geoWeatherReportOnt, false);
    manager.addIRIMapper(autoIRIMapper);

	}

	// TODO needs some fixing possibly
	public void addIndividual(String class_id, String individualName) {
		String prefix = this.uri;
		OWLClass o_class = this.factory.getOWLClass(IRI.create(prefix + "#" + class_id));
		OWLNamedIndividual n_individual = this.factory.getOWLNamedIndividual(IRI.create(individualName));
		OWLClassAssertionAxiom individualClassAssertion = this.factory.getOWLClassAssertionAxiom(o_class, n_individual);
		this.manager.applyChange(new AddAxiom(ontology, individualClassAssertion));
	}

	// TODO needs implementing
	public void addObjectProperty(String class_id, String individualName) {
		String prefix = this.uri;
		OWLClass o_class = this.factory.getOWLClass(IRI.create(prefix + "#" + class_id));
	}

	// TODO needs fixing/implementing
	public void addObjectPropertyValue(String domain_id, String domain_individual, String range_id,
			String range_individual, String property_id) {
	  //Domain_ID         = Location
	  //domain individual = Location1
	  //range_id          = WeatherReport
	  //range_individual  = WeatherReport1
	  //property_ID       = hasWeatherReport
		String prefix = this.uri;
		OWLClass domain = factory.getOWLClass(IRI.create(prefix + "#" + domain_id));
		OWLNamedIndividual domain_nIndividual = this.factory
				.getOWLNamedIndividual(IRI.create(prefix + "#" + domain_individual));
		// Checking if data property already exists if not, then create it
		// if
		// (!this.ontology.containsObjectPropertyInSignature(IRI.create(prefix +
		// "#" + property_id)))
		// this.addDataProperty(domain_id, property_id);
		OWLObjectProperty property = factory.getOWLObjectProperty(IRI.create(prefix + "#" + property_id));
		// Checking if individual already exists if not, then create it
		// if (!this.ontology.containsClassInSignature(IRI.create(prefix + "#" +
		// range_id)))
		// this.addClass(domain_id, range_id);
		OWLClass range = this.factory.getOWLClass(IRI.create(prefix + "#" + range_id));
		OWLNamedIndividual range_nIndividual = this.factory
				.getOWLNamedIndividual(IRI.create(prefix + "#" + range_individual));
		OWLAxiom propertyDomainAxiom = this.factory.getOWLObjectPropertyDomainAxiom(property, domain);
		manager.applyChange(new AddAxiom(ontology, propertyDomainAxiom));
		OWLObjectPropertyRangeAxiom propertyRangeAxiom = this.factory.getOWLObjectPropertyRangeAxiom(property, range);
		manager.applyChange(new AddAxiom(ontology, propertyRangeAxiom));
		OWLAxiom propertyValue = this.factory.getOWLObjectPropertyAssertionAxiom(property, domain_nIndividual,
				range_nIndividual);
		manager.applyChange(new AddAxiom(ontology, propertyValue));
	}

	public void addDataProperty(String domain_id, String d_propertyName, String type) {
		OWLDatatype dataType = determineDataType(type);
		String prefix = this.uri;
		OWLClass domain = factory.getOWLClass(IRI.create(prefix + "#" + domain_id));
		OWLDataProperty d_property = this.factory.getOWLDataProperty(IRI.create(prefix + "#" + d_propertyName));
		OWLAxiom axiom = factory.getOWLDataPropertyDomainAxiom(d_property, domain);
		manager.applyChange(new AddAxiom(this.ontology, axiom));
		OWLDataPropertyRangeAxiom d_propertyRangeAxiom = this.factory.getOWLDataPropertyRangeAxiom(d_property,
				dataType);
		manager.applyChange(new AddAxiom(ontology, d_propertyRangeAxiom));

	}

	public void addDataPropertyValue(String domain_id, String individual_id, String property_id, String d_propertyValue,
			String type) {

		String prefix = this.uri;

		OWLDatatype dataType = null;
		OWLClass domain = factory.getOWLClass(IRI.create(prefix + "#" + domain_id));
		// Checking if data property already exists if not, then create it
		if (!this.ontology.containsDataPropertyInSignature(IRI.create(prefix + "#" + property_id)))
			this.addDataProperty(domain_id, property_id, type);
		OWLDataProperty property = factory.getOWLDataProperty(IRI.create(prefix + "#" + property_id));
//		 Checking if individual already exists if not, then create it
		 if (!this.ontology.containsIndividualInSignature(IRI.create(prefix +
		 "#" + individual_id)))
		 this.addIndividual(domain_id, individual_id);
		OWLNamedIndividual n_individual = this.factory.getOWLNamedIndividual(IRI.create(prefix + "#" + individual_id));
		OWLClassAssertionAxiom domainAssertionAxiom = this.factory.getOWLClassAssertionAxiom(domain, n_individual);
		manager.applyChange(new AddAxiom(ontology, domainAssertionAxiom));
		OWLAxiom propertyIndividualAxiom = null;
		if (type.equals("string")){
			propertyIndividualAxiom = this.factory.getOWLDataPropertyAssertionAxiom(property, n_individual,
					d_propertyValue);
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_STRING);
		}
		else if (type.equals("float")){
			propertyIndividualAxiom = this.factory.getOWLDataPropertyAssertionAxiom(property, n_individual,
					Float.parseFloat(d_propertyValue));
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_FLOAT);
		}
		else if (type.equals("int")){
			propertyIndividualAxiom = this.factory.getOWLDataPropertyAssertionAxiom(property, n_individual,
					Integer.parseInt(d_propertyValue));
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_INT);
		}
		else if (type.equals("double")){
			propertyIndividualAxiom = this.factory.getOWLDataPropertyAssertionAxiom(property, n_individual,
					Double.parseDouble(d_propertyValue));
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_DOUBLE);
		}
		// Employee Individual
		manager.applyChange(new AddAxiom(ontology, propertyIndividualAxiom));
		OWLDataPropertyRangeAxiom propertyRangeAxiom = this.factory.getOWLDataPropertyRangeAxiom(property, dataType);
		manager.applyChange(new AddAxiom(ontology, propertyRangeAxiom));
	}

	public OWLDatatype determineDataType(String type) {
		OWLDatatype dataType = null;
		if (type.equals("string"))
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_STRING);
		else if (type.equals("float"))
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_FLOAT);
		else if (type.equals("int"))
			dataType = OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_INT);
		return dataType;
	}

	public void saveChanges(String output_file) throws OWLOntologyStorageException, IOException {
		File output = new File(output_file);
		this.manager.saveOntology(this.ontology);
		output.createNewFile();
		FileOutputStream outputStream = new FileOutputStream(output);
		manager.saveOntology(ontology, outputStream);
		System.out.println();
		System.out.println("Saved ontology into: " + output_file);
	}

	public OWLOntologyManager getManager() {
		return manager;
	}

	public OWLOntology getOntology() {
		return ontology;
	}

	public Set<OWLClass> getClasses() {
		return classes;
	}

	public Set<OWLNamedIndividual> getIndividuals() {
		return individuals;
	}

	public Set<OWLDataProperty> getDataProperties() {
		return dataProperties;
	}

	public Set<OWLObjectProperty> getObjectProperties() {
		return objectProperties;
	}
}
