package cs.utep.owlApi_test;

import java.util.Set;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;

public class OntologyReader {
	public OntologyReader() {

	}

	public void showContents(OntologyManager manager) {
		System.out.println("/************************** OPENING READER **************************/");
		System.out.println();
		System.out.println("ONTOLOGY: " + manager.getOntologyName());
		printIndividuals(manager);
		printClasses(manager);
		printObjectProperties(manager);
		printDataProperties(manager);
		System.out.println();
		System.out.println("/************************** CLOSING READER **************************/");
	}

	public void printObjectProperties(OntologyManager manager) {
		System.out.println("OBJECT PROPERTIES(" + manager.getObjectProperties().size() + ")");
		for (OWLObjectProperty o : manager.getObjectProperties())
			System.out.println("	" + o);
		System.out.println("---------------------------------------------------------------");

	}

	public void printDataProperties(OntologyManager manager) {
		System.out.println("DATA PROPERTIES(" + manager.getDataProperties().size() + ")");
		for (OWLDataProperty d : manager.getDataProperties())
			System.out.println("	" + d);
		System.out.println("---------------------------------------------------------------");

	}

	public void printClasses(OntologyManager manager) {
		System.out.println("CLASSES: (" + manager.getClasses().size() + ")");
		for (OWLClass c : manager.getClasses())
			System.out.println("	" + c);
		System.out.println("---------------------------------------------------------------");
	}

	public void printIndividuals(OntologyManager manager) {
		System.out.println("INDIVIDUALS: (" + manager.getIndividuals().size() + ")");
		for (OWLNamedIndividual i : manager.getIndividuals()) {
			System.out.println("	" + i);
		}
		System.out.println("---------------------------------------------------------------");
	}
}
