package cs.utep.owlApi_test;

import java.io.IOException;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class Example {
  public static void main(String[] args) throws OWLOntologyStorageException, IOException, OWLOntologyCreationException {
    OntologyReader reader = new OntologyReader();
    // Creating our ontology manager and
    OntologyManager source_ontology = new OntologyManager("OurOntology", "OurOntology.owl", "file:///");
    reader.showContents(source_ontology);
    // // Adding parsed data to ontology
     System.out.println();
    // Creating Ontology Manager for our output
    source_ontology.addIndividual("Employee", "Employee3");
//    source_ontology.addIndividual("Employee", "Empleado");
    source_ontology.addDataProperty("Employee", "isHardworker", "float");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "isReal", "Erick", "string");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "hasFirstName", "Garcia", "string");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "isHardworker", "3", "float");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "isHardworker", "3", "int");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "isHardworker", "3", "string");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "isHardworker", "3", "double");
    source_ontology.addObjectPropertyValue("Employee", "Employee76", "Department", "Department1", "worksAt");
    source_ontology.addDataPropertyValue("Employee", "Employee76", "isTheBestThatNoOneEverWas", "ofCourse", "string");
//    reader.showContents(source_ontology);
    source_ontology.saveChanges("OurOntology.owl");
  }
}
