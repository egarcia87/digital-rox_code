package cs.utep.quakemlParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URI;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.vocab.OWL2Datatype;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import uk.ac.manchester.cs.owl.owlapi.OWL2DatatypeImpl;


public class quakeMLParser 
{
	public static void main( String[] args )
	{
//		String ontFile = "OurOntology.owl";
//
//		String prefix = "file:///";
//
//		URI basePhysicalURI = URI.create(prefix + ontFile.replace("\\", "/"));
//		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

//		try 
//		{
//			OWLOntology ontology = manager.createOntology(IRI.create(basePhysicalURI));
//			OWLDataFactory factory = manager.getOWLDataFactory();
//
//			//Adding Employee class
//			//What is an IRI? https://www.w3.org/TR/owl2-syntax/#IRIs
//			OWLClass employeeClass = factory.getOWLClass(IRI.create(prefix + ontFile + "#Employee"));
//			OWLAxiom employeeClassAxiom = factory.getOWLDeclarationAxiom(employeeClass);
//			manager.addAxiom(ontology, employeeClassAxiom);
//
//			OWLClass departmentClass = factory.getOWLClass(IRI.create(prefix + ontFile + "#Department"));
//			OWLAxiom departmentClassAxiom = factory.getOWLDeclarationAxiom(departmentClass);
//			manager.addAxiom(ontology, departmentClassAxiom);
//
//			//Adding hasFirstName data property 
//			OWLDataProperty hasFirstNameProperty = factory.getOWLDataProperty(IRI.create(prefix + ontFile + "#hasFirstName"));
//
//			OWLAxiom hasFirstNamePropertyAxiom = factory.getOWLDataPropertyDomainAxiom(hasFirstNameProperty, employeeClass);
//			manager.applyChange(new AddAxiom(ontology,hasFirstNamePropertyAxiom));
//
//
//			OWLDatatype stringOwlDataType =  OWL2DatatypeImpl.getDatatype(OWL2Datatype.XSD_STRING);
//			OWLDataPropertyRangeAxiom hasNameRangeAxiom = factory.getOWLDataPropertyRangeAxiom(hasFirstNameProperty, stringOwlDataType);
//			manager.applyChange(new AddAxiom(ontology,hasNameRangeAxiom));
//
//			//Adding Object Property
//			OWLObjectProperty worksAtObjectProperty = factory.getOWLObjectProperty(IRI.create(prefix + ontFile + "#worksAt"));
//			OWLAxiom worksAtObjectPropertyDomainAxiom = factory.getOWLObjectPropertyDomainAxiom(worksAtObjectProperty, employeeClass);
//			manager.applyChange(new AddAxiom(ontology,worksAtObjectPropertyDomainAxiom));
//
//			OWLObjectPropertyRangeAxiom worksAtObjectPropertyRangeAxiom = factory.getOWLObjectPropertyRangeAxiom(worksAtObjectProperty, departmentClass);
//			manager.applyChange(new AddAxiom(ontology,worksAtObjectPropertyRangeAxiom));
//
//			//Employee
//			OWLNamedIndividual employeeIndividual = factory.getOWLNamedIndividual(IRI.create("Employee1"));
//			OWLClassAssertionAxiom employeeClassAssertion = factory.getOWLClassAssertionAxiom(employeeClass, employeeIndividual);
//			manager.applyChange(new AddAxiom(ontology,employeeClassAssertion));
//
//			OWLNamedIndividual departmentIndividual = factory.getOWLNamedIndividual(IRI.create("Department1"));
//			OWLClassAssertionAxiom departmentClassAssertion = factory.getOWLClassAssertionAxiom(departmentClass, departmentIndividual);
//			manager.applyChange(new AddAxiom(ontology,departmentClassAssertion));
//
//
//			OWLAxiom hasFirstNameIndividualAxiom = factory.getOWLDataPropertyAssertionAxiom(hasFirstNameProperty, employeeIndividual, "Diego");
//			manager.applyChange(new AddAxiom(ontology, hasFirstNameIndividualAxiom));
//
//			OWLAxiom worksAtAxiom = factory.getOWLObjectPropertyAssertionAxiom(worksAtObjectProperty, employeeIndividual, departmentIndividual);
//			manager.applyChange(new AddAxiom(ontology, worksAtAxiom));

			try 
			{

				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = docFactory.newDocumentBuilder();

				//Document doc = builder.parse("C:\\Users\\PeachyJoKiller88\\Dropbox\\DigitalGeo\\2 - XML");

				// create a new document from input source
				FileInputStream fis = new FileInputStream("C:\\Users\\PeachyJoKiller88\\Desktop\\quakeml.xml");
				InputSource is = new InputSource(fis);
				Document doc = builder.parse(is);

				// get the first element
				Element element = doc.getDocumentElement();

				// get all child nodes
				NodeList nodes = element.getChildNodes();

				// print the text content of each child
				for (int i = 0; i < nodes.getLength(); i++) {
					//System.out.println("" + nodes.item(i).getTextContent());
				}

				NodeList description = doc.getElementsByTagName("description");
				for (int i = 0; i < description.getLength(); ++i)
				{
					Element currentDesc = (Element) description.item(i);

					NodeList textList = currentDesc.getElementsByTagName("text");
					//Element text = (Element)textList.item(0);//Get the first element of the list since theres one only
					String textValue = ((Element)textList.item(0)).getTextContent();//.getFirstChild().getFirstChild().getNodeValue();//Get the acual value
					//String descText = text.getAttribute("text");
					//String conditionText = text.getFirstChild().getFirstChild().getNodeValue();//Get the acual value

					//Element type = (Element) description.item(i);
					//String descType = type.getAttribute("type");
					//String descriptionType = type.getFirstChild().getFirstChild().getNodeValue();//Get the acual value

					NodeList typeList = currentDesc.getElementsByTagName("type");
					//Element text = (Element)textList.item(0);//Get the first element of the list since theres one only
					String typeValue = ((Element)typeList.item(0)).getTextContent();

					System.out.println("Description:");
					System.out.println(" Text:" + textValue);
					System.out.println(" Type:" + typeValue);
					//System.out.println("Description:" + type);

				}

				NodeList magnitude = doc.getElementsByTagName("magnitude");
				for (int i = 0; i < magnitude.getLength(); ++i)
				{
					Element currentMag = (Element) magnitude.item(i);

					NodeList magList = currentMag.getElementsByTagName("mag");
					String textValue = ((Element)magList.item(0)).getTextContent();//.getFirstChild().getFirstChild().getNodeValue();//Get the acual value


					NodeList textList = currentMag.getElementsByTagName("type");
					String typeValue = ((Element)textList.item(0)).getTextContent();

					NodeList originIDList = currentMag.getElementsByTagName("originID");
					String originIDValue = ((Element)originIDList.item(0)).getTextContent();//Get the actual value

					NodeList evaluationModeList = currentMag.getElementsByTagName("evaluationMode");
					String evaluationModeValue = ((Element)evaluationModeList.item(0)).getTextContent();//Get the actual value

					NodeList evaluationStatusList = currentMag.getElementsByTagName("evaluationStatus");
					String evaluationStatusValue = ((Element)evaluationStatusList.item(0)).getTextContent();//Get the actual value

					NodeList creationInfoList = currentMag.getElementsByTagName("creationInfo");
					String creationInfoValue = ((Element)creationInfoList.item(0)).getTextContent();//Get the actual value

					System.out.println("Magnitude Value:" + textValue);
					System.out.println(" Magnitude Type:" + typeValue);
					System.out.println(" OriginID:" + originIDValue);
					System.out.println(" evaluationMode:" + evaluationModeValue);
					System.out.println(" evaluationStatus:" + evaluationStatusValue);
					System.out.println(" creationInfo:" + creationInfoValue);
					//System.out.println("Description:" + type);

				}

				NodeList origin = doc.getElementsByTagName("origin");
				for (int i = 0; i < origin.getLength(); ++i)
				{
					//Element currentOrigin = (Element) origin.item(i);

					NodeList originUncertainty = doc.getElementsByTagName("originUncertainty");
					for (int j = 0; j < originUncertainty.getLength(); ++j)
					{
						Element currentOriginUncertainty = (Element) origin.item(j);

						NodeList minHorizontalUncertaintyList = currentOriginUncertainty.getElementsByTagName("minHorizontalUncertainty");
						String minHorizontalUncertaintyValue = ((Element)minHorizontalUncertaintyList.item(0)).getTextContent();

						NodeList maxHorizontalUncertaintyList = currentOriginUncertainty.getElementsByTagName("maxHorizontalUncertainty");
						String maxHorizontalUncertaintyValue = ((Element)maxHorizontalUncertaintyList.item(0)).getTextContent();

						System.out.println("origin:");
						System.out.println(" minHorizontalUncertainty:" + minHorizontalUncertaintyValue);
						System.out.println(" maxHorizontalUncertainty:" + maxHorizontalUncertaintyValue);
						//System.out.println(" Type:" + typeValue);

						NodeList confidenceEllipsoid = doc.getElementsByTagName("confidenceEllipsoid");
						for (int k = 0; k < confidenceEllipsoid.getLength(); ++k)
						{
							Element currentConfidenceEllipsoid = (Element) origin.item(k);

							NodeList semiMajorAxisLengthList = currentConfidenceEllipsoid.getElementsByTagName("semiMajorAxisLength");
							String semiMajorAxisLengthValue = ((Element)semiMajorAxisLengthList.item(0)).getTextContent();

							NodeList semiMinorAxisLengthList = currentConfidenceEllipsoid.getElementsByTagName("semiMinorAxisLength");
							String semiMinorAxisLengthValue = ((Element)semiMinorAxisLengthList.item(0)).getTextContent();

							NodeList semiIntermediateAxisLengthList = currentConfidenceEllipsoid.getElementsByTagName("semiIntermediateAxisLength");
							String semiIntermediateAxisLengthValue = ((Element)semiIntermediateAxisLengthList.item(0)).getTextContent();

							NodeList majorAxisPlungeList = currentConfidenceEllipsoid.getElementsByTagName("majorAxisPlunge");
							String majorAxisPlungeValue = ((Element)majorAxisPlungeList.item(0)).getTextContent();

							NodeList majorAxisAzimuthList = currentConfidenceEllipsoid.getElementsByTagName("majorAxisAzimuth");
							String majorAxisAzimuthValue = ((Element)majorAxisAzimuthList.item(0)).getTextContent();

							NodeList majorAxisRotationList = currentConfidenceEllipsoid.getElementsByTagName("majorAxisRotation");
							String majorAxisRotationValue = ((Element)majorAxisRotationList.item(0)).getTextContent();

							System.out.println("confidenceEllipsoid:");
							System.out.println(" semiMajorAxisLength:" + semiMajorAxisLengthValue);
							System.out.println(" semiMinorAxisLength:" + semiMinorAxisLengthValue);
							System.out.println(" semiIntermediateAxisLength:" + semiIntermediateAxisLengthValue);
							System.out.println(" majorAxisPlunge:" + majorAxisPlungeValue);
							System.out.println(" majorAxisAzimuth:" + majorAxisAzimuthValue);
							System.out.println(" majorAxisRotation:" + majorAxisRotationValue);

							//System.out.println(" Type:" + typeValue);
						}
					}

					NodeList arrival = doc.getElementsByTagName("arrival");
					for (int j = 0; j < arrival.getLength(); ++j)
					{
						Element currentArrival = (Element) arrival.item(j);

						NodeList pickIDList = currentArrival.getElementsByTagName("pickID");
						String pickIDValue = ((Element)pickIDList.item(0)).getTextContent();

						NodeList phaseList = currentArrival.getElementsByTagName("phase");
						String phaseValue = ((Element)phaseList.item(0)).getTextContent();

						NodeList azimuthList = currentArrival.getElementsByTagName("azimuth");
						String azimuthValue = ((Element)azimuthList.item(0)).getTextContent();

						NodeList distanceList = currentArrival.getElementsByTagName("distance");
						String distanceValue = ((Element)distanceList.item(0)).getTextContent();

						NodeList timeResidualList = currentArrival.getElementsByTagName("timeResidual");
						String timeResidualValue = ((Element)timeResidualList.item(0)).getTextContent();

						NodeList timeWeightList = currentArrival.getElementsByTagName("timeWeight");
						String timeWeightValue = ((Element)timeWeightList.item(0)).getTextContent();

						System.out.println("origin:");
						System.out.println(" pickID:" + pickIDValue);
						System.out.println(" phase:" + phaseValue);
						System.out.println(" azimuth:" + azimuthValue);
						System.out.println(" distance:" + distanceValue);
						System.out.println(" timeResidual:" + timeResidualValue);
						System.out.println(" timeWeight:" + timeWeightValue);

					}

					NodeList time = doc.getElementsByTagName("time");
					for (int j = 0; j < time.getLength(); ++j)
					{
						Element currenttime = (Element) time.item(j);
						
						System.out.println("time:");

						NodeList valueList = currenttime.getElementsByTagName("value");
						if(valueList != null && valueList.getLength() > 0)
						{
							String valueValue = ((Element)valueList.item(0)).getTextContent();
							System.out.println(" value:" + valueValue);
						}

						NodeList uncertaintyList = currenttime.getElementsByTagName("uncertainty");
						if(uncertaintyList != null && uncertaintyList.getLength() > 0)
						{
							String uncertaintyValue = ((Element)uncertaintyList.item(0)).getTextContent();
							System.out.println(" uncertainty:" + uncertaintyValue);
						}

						NodeList upperUncertaintyList = currenttime.getElementsByTagName("upperUncertainty");
						if(upperUncertaintyList != null && upperUncertaintyList.getLength() > 0)
						{
							String upperUncertaintyValue = ((Element)upperUncertaintyList.item(0)).getTextContent();
							System.out.println(" upperUncertainty:" + upperUncertaintyValue);
						}
						

						NodeList confidenceLevelList = currenttime.getElementsByTagName("confidenceLevel");
						if(confidenceLevelList != null && confidenceLevelList.getLength() > 0)
						{
							String confidenceLevelValue = ((Element)confidenceLevelList.item(0)).getTextContent();
							System.out.println(" confidenceLevel:" + confidenceLevelValue);
						}
						
					}

					NodeList longitude = doc.getElementsByTagName("longitude");
					for (int j = 0; j < longitude.getLength(); ++j)
					{
						Element currentLongitude = (Element) longitude.item(j);

						NodeList longValueList = currentLongitude.getElementsByTagName("value");
						String longValueValue = ((Element)longValueList.item(0)).getTextContent();

						System.out.println("longitude:");
						System.out.println(" value:" + longValueValue);
					}

					NodeList latitude = doc.getElementsByTagName("latitude");
					for (int j = 0; j < latitude.getLength(); ++j)
					{
						Element currentLatitude = (Element) latitude.item(j);

						NodeList valueList = currentLatitude.getElementsByTagName("value");
						String valueValue = ((Element)valueList.item(0)).getTextContent();

						System.out.println("longitude:");
						System.out.println(" value:" + valueValue);
					}
				}

				NodeList depth = doc.getElementsByTagName("depth");
				for (int j = 0; j < depth.getLength(); ++j)
				{
					Element currentDepth = (Element) depth.item(j);

					NodeList valueList = currentDepth.getElementsByTagName("value");
					String valueValue = ((Element)valueList.item(0)).getTextContent();

					NodeList uncertaintyList = currentDepth.getElementsByTagName("uncertainty");
					String uncertaintyValue = ((Element)uncertaintyList.item(0)).getTextContent();

					NodeList upperUncertaintyList = currentDepth.getElementsByTagName("upperUncertainty");
					String upperUncertaintyValue = ((Element)upperUncertaintyList.item(0)).getTextContent();

					NodeList confidenceLevelList = currentDepth.getElementsByTagName("confidenceLevel");
					String confidenceLevelValue = ((Element)confidenceLevelList.item(0)).getTextContent();

					System.out.println("depth:");
					System.out.println(" value:" + valueValue);
					System.out.println(" uncertainty:" + uncertaintyValue);
					System.out.println(" upperUncertainty:" + upperUncertaintyValue);
					System.out.println(" confidenceLevel:" + confidenceLevelValue);
					//System.out.println(" Type:" + typeValue);
				}

				NodeList quality = doc.getElementsByTagName("quality");
				for (int i = 0; i < quality.getLength(); ++i)
				{
					Element currentQuality = (Element) quality.item(i);

					NodeList associatedPhaseCountList = currentQuality.getElementsByTagName("associatedPhaseCount");
					String associatedPhaseCountValue = ((Element)associatedPhaseCountList.item(0)).getTextContent();//.getFirstChild().getFirstChild().getNodeValue();//Get the acual value

					NodeList associatedStationCountList = currentQuality.getElementsByTagName("associatedStationCount");
					String associatedStationCountValue = ((Element)associatedStationCountList.item(0)).getTextContent();

					NodeList usedStationCountList = currentQuality.getElementsByTagName("usedStationCount");
					String usedStationCountValue = ((Element)usedStationCountList.item(0)).getTextContent();

					NodeList standardErrorList = currentQuality.getElementsByTagName("standardError");
					String standardErrorValue = ((Element)standardErrorList.item(0)).getTextContent();

					NodeList azimuthalGapList = currentQuality.getElementsByTagName("azimuthalGap");
					String azimuthalGapValue = ((Element)azimuthalGapList.item(0)).getTextContent();

					System.out.println("quality:");
					System.out.println(" associatedPhaseCount:" + associatedPhaseCountValue);
					System.out.println(" associatedStationCount:" + associatedStationCountValue);
					System.out.println(" usedStationCount:" + usedStationCountValue);
					System.out.println(" standardError:" + standardErrorValue);
					System.out.println(" azimuthalGap:" + azimuthalGapValue);

				}
				
				NodeList evaluationMode = doc.getElementsByTagName("evaluationMode");
				String evaluationModeValue = ((Element)evaluationMode.item(0)).getTextContent();
				System.out.println("evaluationMode:" + evaluationModeValue);
				
				NodeList evaluationStatus = doc.getElementsByTagName("evaluationStatus");
				String evaluationStatusValue = ((Element)evaluationStatus.item(0)).getTextContent();
				System.out.println("evaluationMode:" + evaluationStatusValue);
				
				NodeList creationInfo = doc.getElementsByTagName("creationInfo");
				String creationInfoValue = ((Element)creationInfo.item(0)).getTextContent();
				System.out.println("creationInfo:" + creationInfoValue);
				
				NodeList pick = doc.getElementsByTagName("pick");
				for (int j = 0; j < pick.getLength(); ++j)
				{
					Element currentpick = (Element) pick.item(j);

					NodeList pickIDList = currentpick.getElementsByTagName("pickID");
					if(pickIDList != null && pickIDList.getLength()> 0)
					{
						String pickIDValue = ((Element)pickIDList.item(0)).getTextContent();
						System.out.println(" pickID:" + pickIDValue);
					}
					

					NodeList timeList = currentpick.getElementsByTagName("time");
					if(timeList != null && timeList.getLength()>0)
					{
						String timeValue = ((Element)timeList.item(0)).getTextContent();
						System.out.println(" time:" + timeValue);
					}
					

					NodeList waveformIDList = currentpick.getElementsByTagName("waveformID");
					String waveformIDValue = ((Element)waveformIDList.item(0)).getTextContent();

					NodeList onsetList = currentpick.getElementsByTagName("onset");
					String onsetValue = ((Element)onsetList.item(0)).getTextContent();

					NodeList phaseHintList = currentpick.getElementsByTagName("phaseHint");
					String phaseHintValue = ((Element)phaseHintList.item(0)).getTextContent();

					NodeList evaluationModeList = currentpick.getElementsByTagName("evaluationMode");
					String evaluationmodeValue = ((Element)evaluationModeList.item(0)).getTextContent();

					NodeList evaluationStatusList = currentpick.getElementsByTagName("evaluationStatus");
					String evaluationstatusValue = ((Element)evaluationStatusList.item(0)).getTextContent();

					
					System.out.println("pick:");
					
					
					System.out.println(" waveformID:" + waveformIDValue);
					System.out.println(" onset:" + onsetValue);
					System.out.println(" phaseHint:" + phaseHintValue);
					System.out.println(" evaluationMode:" + evaluationmodeValue);
					System.out.println(" evaluationStatus:" + evaluationstatusValue);
					
				}
				
				NodeList creationinfo = doc.getElementsByTagName("creationInfo"); //doc.get
				for (int j = 0; j < creationinfo.getLength(); ++j)
				{
					
					Element currentcreationinfo = (Element) creationinfo.item(j);
					if(!currentcreationinfo.getParentNode().getNodeName().equals("eventParameters"))
					{
						//System.out.println("parent:" + currentcreationinfo.getParentNode().getNodeName());
						continue;
					}
					
					System.out.println("creationinfo:");
					
					NodeList agencyIDList = currentcreationinfo.getElementsByTagName("agencyID");
					if(agencyIDList != null && agencyIDList.getLength()>0)
					{
						String agencyIDValue = ((Element)agencyIDList.item(0)).getTextContent();
						System.out.println(" agencyID:" + agencyIDValue);
					}

					NodeList creationTimeList = currentcreationinfo.getElementsByTagName("creationTime");
					if(creationTimeList != null && creationTimeList.getLength()> 0)
					{
						String creationTimeValue = ((Element)creationTimeList.item(0)).getTextContent();
						System.out.println(" creationTime:" + creationTimeValue);
					}
					
				}
			} 
			
			catch (Exception e) {
				e.printStackTrace();
			}
			//File file = new File(ontFile);
			//file.createNewFile();

			//FileOutputStream outputStream = new FileOutputStream(file);
			//manager.saveOntology(ontology, outputStream);
			System.out.println("Done Saving");

		//} 
		//catch (Exception e) 
		{
			//e.printStackTrace();
		}

	}


}
