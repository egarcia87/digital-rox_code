package cs.utep.xmlParser;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GeoWeatherParser {
  private Hashtable<String, ArrayList<String>> weatherReports;
  private Hashtable<String, ArrayList<String>> locations;
  private Hashtable<String, ArrayList<String>> windSpeeds;
  private Hashtable<String, ArrayList<String>> temperatures;

  public GeoWeatherParser(String xml_fileName) {
    weatherReports = new Hashtable<String, ArrayList<String>>();
    locations      = new Hashtable<String, ArrayList<String>>();
    windSpeeds     = new Hashtable<String, ArrayList<String>>();
    temperatures   = new Hashtable<String, ArrayList<String>>();
    File xmlFile   = new File(xml_fileName);
    parse(xmlFile);
  }

  public void parse(File xmlFile) {
    try {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(xmlFile);
      doc.getDocumentElement().normalize();
//      System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

      NodeList nList = doc.getElementsByTagName("record");
//      System.out.println("----------------------------");
      for (int i = 0; i < nList.getLength(); i++) {
        Node nNode = nList.item(i);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          String record = eElement.getAttribute("id");

          //Creating Arraylists for Hashsets
          ArrayList<String> weatherReport_Elements = new ArrayList<String>();
          ArrayList<String> location_Elements      = new ArrayList<String>();
          ArrayList<String> windSpeed_Elements     = new ArrayList<String>();
          ArrayList<String> temperature_Elements   = new ArrayList<String>();

          String station_id = eElement.getElementsByTagName("station_id").item(0).getTextContent();
          String location = eElement.getElementsByTagName("location").item(0).getTextContent();
          String latitude = eElement.getElementsByTagName("latitude").item(0).getTextContent();
          String longitude = eElement.getElementsByTagName("longitude").item(0).getTextContent();
          String weather = eElement.getElementsByTagName("weather").item(0).getTextContent();
          String temp_f = eElement.getElementsByTagName("temp_f").item(0).getTextContent();
          String temp_c = eElement.getElementsByTagName("temp_c").item(0).getTextContent();
          String relative_humidity = eElement.getElementsByTagName("relative_humidity").item(0).getTextContent();
          String pressure_in = eElement.getElementsByTagName("pressure_in").item(0).getTextContent();
          String windDegree = eElement.getElementsByTagName("wind_degrees").item(0).getTextContent();
          String windDirection = eElement.getElementsByTagName("wind_dir").item(0).getTextContent();
          String windSpeed = eElement.getElementsByTagName("wind_mph").item(0).getTextContent();

          //Location
          location_Elements.add("hasLocationName"); //LocationName
          location_Elements.add(location);  
          location_Elements.add("string");
          location_Elements.add("hasLatitude");     //Latitude
          location_Elements.add(latitude);  
          location_Elements.add("float");
          location_Elements.add("hasLongitude");    //Longitude
          location_Elements.add(longitude); 
          location_Elements.add("float"); 

          //WeatherReport
          weatherReport_Elements.add("Station_ID");
          weatherReport_Elements.add(station_id);
          weatherReport_Elements.add("string");
          weatherReport_Elements.add("WeatherType");
          weatherReport_Elements.add(weather);
          weatherReport_Elements.add("string");
          weatherReport_Elements.add("RelativeHumidity");
          weatherReport_Elements.add(relative_humidity);
          weatherReport_Elements.add("float");
          weatherReport_Elements.add("Pressure");
          weatherReport_Elements.add(pressure_in);
          weatherReport_Elements.add("float");
          weatherReport_Elements.add("WindDegree");
          weatherReport_Elements.add(windDegree);
          weatherReport_Elements.add("int");
          weatherReport_Elements.add("WindDirection");
          weatherReport_Elements.add(windDirection);
          weatherReport_Elements.add("string");

          //Wind Speed
          windSpeed_Elements.add("WindSpeed");
          windSpeed_Elements.add(windSpeed);
          windSpeed_Elements.add("float");
          
          //Temperatures
          temperature_Elements.add("Temperature"); //Farenheit
          temperature_Elements.add(temp_f);
          temperature_Elements.add("float");
          temperature_Elements.add("Temperature"); //Celcius
          temperature_Elements.add(temp_c);
          temperature_Elements.add("float");

          //Adding everything to hashsets
          weatherReports.put(record, weatherReport_Elements);
          locations.put(record, location_Elements);
//          windSpeeds.put(record, windSpeed_Elements);
          temperatures.put(record, temperature_Elements);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Hashtable<String, ArrayList<String>> getWindSpeeds() {
    return windSpeeds;
  }

  public Hashtable<String, ArrayList<String>> getTemperatures() {
    return temperatures;
  }

  public Hashtable<String, ArrayList<String>> getWeatherReports() {
    return weatherReports;
  }

  public Hashtable<String, ArrayList<String>> getLocations() {
    return locations;
  }
}